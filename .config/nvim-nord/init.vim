
call plug#begin('~/local/share/nvim/plugged')

Plug 'norcalli/nvim-colorizer.lua'
#Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'junegunn/goyo.vim'
Plug 'shaunsingh/nord.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'

call plug#end()

set noshowmode

colorscheme nord

require('lualine').setup {
  options = {
    -- ... your lualine config
    theme = 'nord'
    -- ... your lualine config
  }
}

#let g:lightline = {
#      \ 'colorscheme': 'wombat',
#      \ 'active': {
#      \   'left': [ [ 'mode', 'paste' ],
#      \             [ 'readonly', 'filename', 'modified' ] ]
#      \ },
#      \ }

:set number

